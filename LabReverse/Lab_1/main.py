import pefile #главная бибилиотека по pe, которая делает dump в виде списка
import itertools #используем один раз для преобразования списка в словарь
pe_input = str(input("Enter path to exe:")) #указываем путь до файла
pe = pefile.PE(pe_input) #Принимаем на вход путь к *.exe
pe_dump = pe.OPTIONAL_HEADER.dump() #Получаем опциональный header нашего exe
pe_dump_list = list() #Создаём пустой лист для дампа
list_name = list() #Создаём пустой лист для редактированного дампа
for i in pe_dump[1::]: #Срезаем заголовок OPTIONAL HEADER, который нам не нужен и помещаем в pe_dump_list
    pe_dump_list.append(i.split())

for i in pe_dump_list: #Делаем список имён и параметров, срез идёт с обратной стороны  и добавляем в список lIst_name
    for j in i[-2::]:
        list_name.append(j)

dict_name_param = dict(itertools.zip_longest(*[iter(list_name)] * 2, fillvalue="")) #Делаем из списка list_name словарь dict_name_param
                                                                               #где у нас ключ это имя, а параметр - это значение
#print("LIST_NAME",list_name) тестовые проверки
#print("PE_DUMP_LIST",pe_dump_list) тестовые проверки
#print("DICT_NAME_PARAM",dict_name_param) тестовые проверки
#1. Поиск имени(ключа) в списке, по которому в словаре находим его Value и проверяем на условие
#2. Добавляем строку по индексу имени в списке
for i in list_name[::2]:
    if i == "Magic:" and dict_name_param[str(i)] == "0x10B":
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Заголовок PE32")
    elif i == 'Magic:' and dict_name_param[str(i)] == "0x107":
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Заголовок прошивки в ПЗУ")
    elif i == 'Magic:' and dict_name_param[str(i)] == "0x20B":
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Заголовок PE32+")
    elif i == 'MajorLinkerVersion:':
        pe_dump_list[int(list_name.index(i)/2)].append("BYTE")
        pe_dump_list[int(list_name.index(i)/2)].append("Старшая цифра номера версии сборщика. Загрузчиком не используется.")
    elif i == 'MinorLinkerVersion:':
        pe_dump_list[int(list_name.index(i)/2)].append("BYTE")
        pe_dump_list[int(list_name.index(i)/2)].append("Младшая цифра номера версии сборщика. Загрузчиком не используется.")
    elif i == 'SizeOfCode:':
        pe_dump_list[int(list_name.index(i)/2)].append("DWORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Сумма размеров всех секций, содержащих програмный код.")
    elif i == 'SizeOfInitializedData:':
        pe_dump_list[int(list_name.index(i)/2)].append("DWORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Сумма размеров всех секций, содержащих инициализированные данные.")
    elif i == 'SizeOfUninitializedData:':
        pe_dump_list[int(list_name.index(i)/2)].append("DWORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Сумма размеров всех секций, содержащих неинициализированные данные.")
    elif i == 'AddressOfEntryPoint:':
        pe_dump_list[int(list_name.index(i)/2)].append("DWORD")
        pe_dump_list[int(list_name.index(i)/2)].append("RVA точки запуска программы. Для драйвера – это адрес DriverEntry, для DLL – адрес DllMain или нуль.")
    elif i == 'BaseOfCode:':
        pe_dump_list[int(list_name.index(i)/2)].append("DWORD")
        pe_dump_list[int(list_name.index(i)/2)].append("RVA начала кода программы.")
    elif i == 'BaseOfData:':
        pe_dump_list[int(list_name.index(i)/2)].append("DWORD")
        pe_dump_list[int(list_name.index(i)/2)].append("RVA начала данных программы. Ненадежное поле, загрузчиком не используется. В PE32+ отсутствует!")
    elif i == 'ImageBase:':
        pe_dump_list[int(list_name.index(i)/2)].append("DWORD/ULONGLONG")
        pe_dump_list[int(list_name.index(i)/2)].append("Предпочтительный базовый адрес программы в памяти, кратный 64 Кб. По умолчанию равен 0x00400000 для EXE-файлов в Windows 9x/NT, 0x00010000 для EXE-файлов в Windows CE и 0x10000000 для DLL. Загрузка программы с этого адреса позволяет обойтись без настройки адресов.")
    elif i == 'SectionAlignment:':
        pe_dump_list[int(list_name.index(i)/2)].append("DWORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Выравнивание в байтах для секций при загрузке в память, большее или равное FileAlignment. По умолчанию равно размеру страницы виртуальной памяти для данного процессора.")
    elif i == 'FileAlignment:':
        pe_dump_list[int(list_name.index(i)/2)].append("DWORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Выравнивание в байтах для секций внутри файла. Должно быть степенью 2 от 512 до 64 Кб включительно. По умолчанию равно 512. Если SectionAlignment меньше размера страницы виртуальной памяти, то FileAlignment должно с ним совпадать.")
    elif i == 'MajorOperatingSystemVersion:':
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Старшая цифра номера версии операционной системы. Загрузчиком не используется.")
    elif i == 'MinorOperatingSystemVersion:':
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Младшая цифра номера версии операционной системы. Загрузчиком не используется.")
    elif i == 'MajorImageVersion:':
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Старшая цифра номера версии данного файла. Загрузчиком не используется.")
    elif i == 'MinorImageVersion:':
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Младшая цифра номера версии данного файла. Загрузчиком не используется.")
    elif i == 'MajorSubsystemVersion:':
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Старшая цифра номера версии подсистемы.")
    elif i == 'MinorSubsystemVersion:':
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Младшая цифра номера версии подсистемы.")
    elif i == 'Reserved1:':
        pe_dump_list[int(list_name.index(i)/2)].append("DWORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Win32VersionValue. Зарезервировано, всегда равно 0.")
    elif i == 'SizeOfImage:':
        pe_dump_list[int(list_name.index(i)/2)].append("DWORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Размер файла в памяти, включая все заголовки. Должен быть кратен SectionAlignment.")
    elif i == 'SizeOfHeaders:':
        pe_dump_list[int(list_name.index(i)/2)].append("DWORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Суммарный размер заголовка и заглушки DOS, заголовка PE и заголовков секций, выравненный на границу FileAlignment. Задает смещение от начала файла до данных первой секции.")
    elif i == 'CheckSum:':
        pe_dump_list[int(list_name.index(i)/2)].append("DWORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Контрольная сумма файла. Проверяется только в Windows NT при загрузке драйверов ядра и нескольких системных DLL. для ее вычисления можно использовать системную функцию CheckSumMappedFile из библиотеки IMAGEHLP.DLL.")
    elif i == 'Subsystem:' and dict_name_param[str(i)] == "0x0":
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Неизвестная подсистема.")
    elif i == 'Subsystem:' and dict_name_param[str(i)] == "0x1":
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Подсистема не требуется, используется драйверами и родными приложениями NT.")
    elif i == 'Subsystem:' and dict_name_param[str(i)] == "0x2":
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Графическая подсистема Windows.")
    elif i == 'Subsystem:' and dict_name_param[str(i)] == "0x3":
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Консольная подсистема Windows")
    elif i == 'Subsystem:' and dict_name_param[str(i)] == "0x5":
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Консольная подсистема OS/2.")
    elif i == 'Subsystem:' and dict_name_param[str(i)] == "0x7":
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Консольная подсистема POSIX.")
    elif i == 'Subsystem:' and dict_name_param[str(i)] == "0x8":
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Родной драйвер Win 9x.")
    elif i == 'Subsystem:' and dict_name_param[str(i)] == "0x9":
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Графическая подсистема Windows CE.")
    elif i == 'Subsystem:' and dict_name_param[str(i)] == "0x10":
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Программа EFI")
    elif i == 'Subsystem:' and dict_name_param[str(i)] == "0x11":
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Драйвер EFI, обеспечивающий загрузочный сервис.")
    elif i == 'Subsystem:' and dict_name_param[str(i)] == "0x12":
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Драйвер EFI времени выполнения.")
    elif i == 'Subsystem:' and dict_name_param[str(i)] == "0x13":
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Прошивка ПЗУ для EFI.")
    elif i == 'Subsystem:' and dict_name_param[str(i)] == "0x14":
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Подсистема Xbox.")
    elif i == 'DllCharacteristics:':
        pe_dump_list[int(list_name.index(i)/2)].append("WORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Дополнительные атрибуты файла.")
    elif i == 'SizeOfStackReserve:':
        pe_dump_list[int(list_name.index(i)/2)].append("DWORD/ULONGLONG")
        pe_dump_list[int(list_name.index(i)/2)].append("Размер стека стартового потока программы в байтах виртуальной памяти. При загрузке в физическую память отображается только SizeOfStackCommit байт, в дальнейшем отображается по одной странице виртуальной памяти. По умолчанию равен 1 Мб.")
    elif i == 'SizeOfStackCommit:':
        pe_dump_list[int(list_name.index(i)/2)].append("DWORD/ULONGLONG")
        pe_dump_list[int(list_name.index(i)/2)].append("Начальный размер стека программы в байтах. По умолчанию равен 4 Кб.")
    elif i == 'SizeOfHeapReserve:':
        pe_dump_list[int(list_name.index(i)/2)].append("DWORD/ULONGLONG")
        pe_dump_list[int(list_name.index(i)/2)].append("Размер кучи программы в байтах. При загрузке в физическую память отображается только SizeOfHeapCommit байт. По умолчанию равен 1 Мб. Во всех 32-разрядных версиях Windows куча ограничена только размером виртуальной памяти.")
    elif i == 'SizeOfHeapCommit:':
        pe_dump_list[int(list_name.index(i)/2)].append("DWORD/ULONGLONG")
        pe_dump_list[int(list_name.index(i)/2)].append("Начальный размер кучи программы в байтах. По умолчанию равен 4 Кб.")
    elif i == 'SizeOfHeapCommit:':
        pe_dump_list[int(list_name.index(i)/2)].append("DWORD/ULONGLONG")
        pe_dump_list[int(list_name.index(i)/2)].append("Начальный размер кучи программы в байтах. По умолчанию равен 4 Кб.")
    elif i == 'NumberOfRvaAndSizes:':
        pe_dump_list[int(list_name.index(i)/2)].append("DWORD")
        pe_dump_list[int(list_name.index(i)/2)].append("Количество описателей каталогов данных. На текущий момент всегда равно 16.")
    else:
        print("Can't find name: "+i+" index: "+str(int(list_name.index(i)/2))+" !")

for i in pe_dump_list:
    print(i)
